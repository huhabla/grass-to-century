#!/bin/sh

export GRASS_OVERWRITE=1

g.region res3=10 b=0 t=1200 n=80 s=0 w=0 e=120 -p3
r3.mapcalc expr="prec = rand(0, 500)"
r3.mapcalc expr="tmin = if(row() != 6, rand(-50, 20), null())"
r3.mapcalc expr="tmax = rand(-20, 50)"

time r3.out.wth prec=prec tmin=tmin tmax=tmax basename=climate \
    names=names.txt region=region.txt year=1950
