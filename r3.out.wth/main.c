/****************************************************************************
 *
 * MODULE:       r3.out.wth
 *
 * AUTHOR(S):   Soeren Gebbert
 *   			Based on r.out.bin from: Bob Covill and Glynn Clements
 *
 * PURPOSE:     Exports precipitation and min/max temperature GRASS 3D raster maps to CENTURY weather files.
 *
 * COPYRIGHT:   (C) 2012 by the GRASS Development Team
 *
 *               This program is free software under the GNU General Public
 *   	    	License (>=v2). Read the file COPYING that comes with GRASS
 *   	    	for details.
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <grass/gis.h>
#include <grass/raster.h>
#include <grass/raster3d.h>
#include <grass/glocale.h>

/* Global cache for prec. tmin, tmax */
float *prec_cache;
float *tmin_cache;
float *tmax_cache;

static void write_wth_file(char *basename, FILE *namesfile, int col, int row, 
                           void* prec_map, void *tmin_map, void *tmax_map, 
                           RASTER3D_Region *region, int start_year);

int main(int argc, char *argv[]) {
    struct GModule *module;
    struct {
            struct Option *prec;
            struct Option *tmin;
            struct Option *tmax;
            struct Option *basename;
            struct Option *namesfile;
            struct Option *regionfile;
            struct Option *year;
    } parm;
    void *prec_map;
    void *tmin_map;
    void *tmax_map;
    int col, row;
    RASTER3D_Region region;
    FILE *namesfile;
    FILE *regionfile;
    int year;

    G_gisinit(argv[0]);

    module = G_define_module();
    G_add_keyword(_("raster3d"));
    G_add_keyword(_("export"));
    G_add_keyword(_("CENTURY"));
    module->description = _("Exports precipitation and min/max temperature GRASS 3D raster maps to CENTURY weather files.");

    /* Define the different options */

    parm.prec = G_define_standard_option(G_OPT_R3_INPUT);
    parm.prec->key = "prec";
    parm.tmin = G_define_standard_option(G_OPT_R3_INPUT);
    parm.tmin->key = "tmin";
    parm.tmax = G_define_standard_option(G_OPT_R3_INPUT);
    parm.tmax->key = "tmax";

    parm.basename = G_define_standard_option(G_OPT_F_OUTPUT);
    parm.basename->key = "basename";

    parm.namesfile = G_define_standard_option(G_OPT_F_OUTPUT);
    parm.namesfile->key = "namesfile";
    parm.regionfile = G_define_standard_option(G_OPT_F_OUTPUT);
    parm.regionfile->key = "regionfile";

    parm.year = G_define_option();
    parm.year->key = "year";
    parm.year->type = TYPE_INTEGER;
    parm.year->required = YES;
    parm.year->description = "The start year";

    if (G_parser(argc, argv))
            exit(EXIT_FAILURE);

    if (NULL == G_find_raster3d(parm.prec->answer, ""))
            Rast3d_fatal_error(_("3D raster map <%s> not found"),
                            parm.prec->answer);

    if (NULL == G_find_raster3d(parm.tmin->answer, ""))
            Rast3d_fatal_error(_("3D raster map <%s> not found"),
                            parm.tmin->answer);


    if (NULL == G_find_raster3d(parm.tmax->answer, ""))
            Rast3d_fatal_error(_("3D raster map <%s> not found"),
                            parm.tmax->answer);

    year = atoi(parm.year->answer);

    /* Initiate the default settings */
    Rast3d_init_defaults();

    /* Figure out the current region settings */
    Rast3d_get_window(&region);

    /* Allocate the cache */
    prec_cache = (float*)G_calloc(region.depths, sizeof(float));
    tmin_cache = (float*)G_calloc(region.depths, sizeof(float));
    tmax_cache = (float*)G_calloc(region.depths, sizeof(float));

    /* Check if the region */
    if(region.depths % 12 != 0)
        Rast3d_fatal_error(_("The number of depths must be a multiple of 12"));

    /* Open the precipitation map and use Z cache mode */
    prec_map = Rast3d_open_cell_old(parm.prec->answer,
                    G_find_raster3d(parm.prec->answer, ""), &region,
                    RASTER3D_TILE_SAME_AS_FILE, RASTER3D_USE_CACHE_XZ);

    if (prec_map == NULL)
            Rast3d_fatal_error(_("Unable to open 3D raster map <%s>"),
                            parm.prec->answer);

    /* Open the precipitation map and use Z cache mode */
    tmin_map = Rast3d_open_cell_old(parm.tmin->answer,
                    G_find_raster3d(parm.tmin->answer, ""), &region,
                    RASTER3D_TILE_SAME_AS_FILE, RASTER3D_USE_CACHE_XZ);

    if (tmin_map == NULL)
            Rast3d_fatal_error(_("Unable to open 3D raster map <%s>"),
                            parm.tmin->answer);

    /* Open the precipitation map and use Z cache mode */
    tmax_map = Rast3d_open_cell_old(parm.tmax->answer,
                    G_find_raster3d(parm.tmax->answer, ""), &region,
                    RASTER3D_TILE_SAME_AS_FILE, RASTER3D_USE_CACHE_XZ);

    if (tmax_map == NULL)
            Rast3d_fatal_error(_("Unable to open 3D raster map <%s>"),
                            parm.tmax->answer);

    /*Open the names file */
    namesfile = fopen(parm.namesfile->answer, "w+");
    if(namesfile == NULL)
        Rast3d_fatal_error(_("Unable to open file <%s>"),
                            parm.namesfile->answer);

    regionfile = fopen(parm.regionfile->answer, "w+");
    if(regionfile == NULL)
        Rast3d_fatal_error(_("Unable to open file <%s>"),
                            parm.regionfile->answer);


    fprintf(regionfile,"north=%f\n", region.north);
    fprintf(regionfile,"south=%f\n", region.south);
    fprintf(regionfile,"east=%f\n", region.east);
    fprintf(regionfile,"west=%f\n", region.west);
    fprintf(regionfile,"top=%f\n", region.top);
    fprintf(regionfile,"bottom=%f\n", region.bottom);
    fprintf(regionfile,"rows=%d\n", region.rows);
    fprintf(regionfile,"cols=%d\n", region.cols);
    fprintf(regionfile,"depths=%d\n", region.depths);
    fprintf(regionfile,"ewres=%f\n", region.ew_res);
    fprintf(regionfile,"nsres=%f\n", region.ns_res);
    fprintf(regionfile,"tbres=%f\n", region.tb_res);

    for(col = 0; col < region.cols; col++) {
        G_percent(col, region.cols, 1);
        for(row = 0; row < region.rows; row++) {
            write_wth_file(parm.basename->answer, namesfile, col, row,
                           prec_map, tmin_map, tmax_map, &region, year);
        }
    }


    Rast3d_close(prec_map);
    Rast3d_close(tmin_map);
    Rast3d_close(tmax_map);

    if(fclose(namesfile) != 0)
        Rast3d_fatal_error(_("Unable to close file <%s>"),
                            parm.namesfile->answer);

    if(fclose(regionfile) != 0)
        Rast3d_fatal_error(_("Unable to close file <%s>"),
                            parm.regionfile->answer);

    G_percent(1,1,1);

    return EXIT_SUCCESS;
}

static void write_wth_file(char *basename, FILE *namesfile, int col, int row, 
                           void* prec_map, void *tmin_map, void *tmax_map, 
                           RASTER3D_Region *region, int start_year)
{
    char filename[GNAME_MAX];
    int depth;
    FILE *wthfile;
    FCELL value;
    int year, num_years;

    /* Read the voxel data into the cache, do not write the weather file
     * in case single value is null */
    for(depth = 0; depth < region->depths; depth++) {
            Rast3d_get_value(prec_map, col, row, depth, &value, FCELL_TYPE);
            prec_cache[depth] = value;

            if(Rast_is_f_null_value(&value))
                return;

            Rast3d_get_value(tmin_map, col, row, depth, &value, FCELL_TYPE);
            tmin_cache[depth] = value;

            if(Rast_is_f_null_value(&value))
                return;

            Rast3d_get_value(tmax_map, col, row, depth, &value, FCELL_TYPE);
            tmax_cache[depth] = value;

            if(Rast_is_f_null_value(&value))
                return;
    }

    G_snprintf(filename, GNAME_MAX, "%s_C%05i_R%05i.wth", basename, col, row);
    fprintf(namesfile, "%s\n", filename);

    wthfile = fopen(filename, "w+");
    if(namesfile == NULL)
        Rast3d_fatal_error(_("Unable to open file <%s>"), filename);

    num_years = region->depths / 12;

    for(year = 0; year < num_years; year++){
        fprintf(wthfile, "prec  %i", start_year + year);
        for(depth = 0; depth < 12; depth++) {
            fprintf(wthfile, "% 7.2f", prec_cache[(year * depth) + depth]);
        }
        fprintf(wthfile, "\n");

        fprintf(wthfile, "tmin  %i", start_year + year);
        for(depth = 0; depth < 12; depth++) {
            fprintf(wthfile, "% 7.2f", tmin_cache[(year * depth) + depth]);
        }
        fprintf(wthfile, "\n");

        fprintf(wthfile, "tmax  %i", start_year + year);
        for(depth = 0; depth < 12; depth++) {
            fprintf(wthfile, "% 7.2f", tmax_cache[(year * depth) + depth]);
        }
        fprintf(wthfile, "\n");
    }

    if(fclose(wthfile) != 0)
        Rast3d_fatal_error(_("Unable to close file <%s>"), filename);
}

